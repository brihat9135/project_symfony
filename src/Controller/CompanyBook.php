<?php
/**
 * @author  Brihat
 * Data: 12/21/2018
 * CompanyBook is a controller that guides the routes and return required infromation for the twig
 *
 */

namespace App\Controller;

use App\Entity\Company;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class CompanyBook extends AbstractController
{
    /**
     * @Route("/company", name="all_companies")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function index()
    {
        $all_persons = Company::getAllCompany();

        return $this->render(
            'article/company.html.twig',
            array('companies' => $all_persons)
        );
    }



    /**
     *function to get company by using company id
     * @Route("/company/{id}", name="company_show")
     */
    public function getByComId($id)
    {
        $all_persons = Company::getCompanybyId($id);

        return $this->render(
            'article/company.html.twig',
            array('companies' => $all_persons)
        );
    }



    /**
     *function to create new company using the twig form
     * @Route("/add_company", name="add_company")
     */

    public function new(Request $request)
    {
        // creates a task and gives it some dummy data for this example
        $task = new Company();

        $form = $this->createFormBuilder($task)
            ->add('company_name', TextType::class)
            ->add('address', TextType::class)
            ->add('city', TextType::class)
            ->add('state', TextType::class)
            ->add('zip', TextType::class)
            ->add('phone', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Company'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $task->addCompany();
            return $this->redirectToRoute('all_companies');
        }

        return $this->render('article/companyform.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     *function to delete company by given name, done by clicking the delete button on the company page
     * @Route("/delete_compnay/{name}", name="delete_company")
     *
     */
    public function deleteCompanyByName($name){
        Company::deleteCompany($name);
        return $this->redirectToRoute('all_companies');
    }



    /**
     *function to edit company by it name, it doesn't allow user to change the company id only allows them to edit other information
     * @Route("/edit_company/{name}", name="edit_company")
     *
     */
    public function editCompanyByName($name, Request $request){
        $company_values = Company::getCompanybyName($name);
        $company = new Company();
        $company->setCompanyName($company_values[0]['name']);
        $company->setAddress($company_values[0]['address']);
        $company->setCity($company_values[0]['city']);
        $company->setState($company_values[0]['state']);
        $company->setZip($company_values[0]['zip']);
        $company->setPhone($company_values[0]['phone']);

        $form = $this->createFormBuilder($company)
            ->add('company_name', TextType::class)
            ->add('address', TextType::class)
            ->add('city', TextType::class)
            ->add('state', TextType::class)
            ->add('zip', TextType::class)
            ->add('phone', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Edit Company'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company = $form->getData();
            $company->updateCompany($name);
            return $this->redirectToRoute('all_companies');
        }

        return $this->render('article/editCompany.html.twig', array(
            'form' => $form->createView(),
        ));

        return $this->redirectToRoute('all_companies');
    }

}