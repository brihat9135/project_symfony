<?php
/**
 * Created by PhpStorm.
 * User: cs
 * Date: 12/20/18
 * Time: 12:49 AM
 */

namespace App\Controller;


use App\Entity\Person;
use App\Entity\Company;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class PersonBook extends AbstractController
{
    /**
     * @Route("/contacts", name="all_contacts")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $all_persons = Person::getAllPerson();

        return $this->render(
            'article/person.html.twig',
            array('peopleList' => $all_persons)
        );
    }




    /**
     *Function to get person using person id
     * @Route("/contacts/{id}")
     */
    public function getByPersonId($id)
    {
        $all_persons = Person::getPersonByPersonId($id);

        return $this->render(
            'article/person.html.twig',
            array('peopleList' => $all_persons)
        );
    }





    /**
     *Function to get person using company id
     * @Route("/company/{id}/contact")
     */

    public function getbyCompanyID($id)
    {
        $all_persons = Person::getPersonByCompanyId($id);
        return $this->render('article/person.html.twig',
            array('peopleList' => $all_persons)
        );
    }


    /**
     *function to delete company by given name, done by clicking the delete button on the company page
     * @Route("/delete_person/{name}", name="delete_person")
     *
     */
    public function deletePersonByName($name){
        Person::deletePerson($name);
        return $this->redirectToRoute('all_contacts');
    }


    /**
     *function to edit person by it's name, it doesn't allow user to change the person id only allows them to edit other information
     * @Route("/edit_person/{name}", name="edit_person")
     *
     */
    public function editPersonByName($name, Request $request){
        $person_values = Person::getPersonByName($name);
        $person = new Person();
        $person->setfirstname($person_values[0]['first_name']);
        $person->setlastname($person_values[0]['last_name']);
        $person->setcompany($person_values[0]['company']);
        $person->setphone($person_values[0]['phone']);
        $person->setemail($person_values[0]['email']);

        $companies = Company::getAllCompany();
        $data = array();
        foreach ($companies as $com) {

            array_push($data, $com{'name'});
        }


        $data = array_combine($data, $data);

        $form = $this->createFormBuilder($person)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('phone', TextType::class)
            ->add('email', TextType::class)
            ->add('company', ChoiceType::class, array(
                'choices' => $data,
                'choice_value' => function($value) {
                    return $value;
                }
            ))

            ->add('save', SubmitType::class, array('label' => 'Update Person'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company = $form->getData();
            $company->updatePerson($name);
            return $this->redirectToRoute('all_contacts');
        }

        return $this->render('article/editPerson.html.twig', array(
            'form' => $form->createView(),
        ));

        return $this->redirectToRoute('all_contacts');
    }


    /**
     *function to create new person using the twig form
     * @Route("/add_person", name="add_person")
     */



    public function new(Request $request)
    {
        // creates a task and gives it some dummy data for this example
        $task = new Person();


        //get all company and keep them inside an array
        $companies = Company::getAllCompany();
        $data = array();
        foreach ($companies as $com) {

            array_push($data, $com{'name'});
        }


        $data = array_combine($data, $data);

        $form = $this->createFormBuilder($task)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('phone', TextType::class)
            ->add('email', TextType::class)
            ->add('company', ChoiceType::class, array(
                'choices' => $data,
                'choice_value' => function($value) {
                    return $value;
                }
            ))
            ->add('save', SubmitType::class, array('label' => 'Create Person'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $task->addPerson();
            return $this->redirectToRoute('all_contacts');
        }

        return $this->render('article/personform.html.twig', array(
            'form' => $form->createView(),
        ));
    }



}