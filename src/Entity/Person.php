<?php

/**
 * @author Brihat
 * Person Entity, which also contains methods to connect to database to get required infromations
 */


namespace App\Entity;
use App\Entity\Company;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\DriverManager;
use App\Entity\databaseConnection;


class Person
{
    protected $firstname;
    protected $lastname;
    protected $company;
    protected $phone;
    protected $email;


    /**
     * static funtion to get personn by company id
     * @return sql return of the person infromation
     */


    static function getPersonByCompanyId($id){
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $sql = "
          SELECT 
            p.*,
            c.name as company
          FROM public.person p 
          JOIN public.company c ON c.company_id = p.com_id
          WHERE p.com_id=?
        ";
        $stmt = $connection->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        return $stmt;
    }


    /**
     * static function to get person by person id
     * @return sql result
     */

    static function getPersonByPersonId($id){
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $sql = "
          SELECT 
            p.*,
            c.name as company
          FROM public.person p
          JOIN public.company c ON c.company_id = p.com_id
          WHERE p.person_id=:id
        ";
        $stmt = $connection->prepare($sql);
        $stmt->bindValue("id", $id);
        $stmt->execute();
        return $stmt;
    }

    /**
     * Static function to gell the person
     * @return sql return of the list of person
     */

    static function getAllPerson()
    {

        $db = new databaseConnection();
        $connection = $db->getConnection();
        $sql = "
            SELECT
              p.*,
              c.name as company
            FROM public.person p
            JOIN public.company c ON c.company_id = p.com_id
            
        ";
        $users = $connection->fetchAll($sql);
        return $users;

    }

    /**
     * static function to deleteperson by name, this should be changed to id so that multiple person doesn't get deleted
     * @param $name
     * @throws \Doctrine\DBAL\DBALException
     */



    static function deletePerson($name)
    {
        $sql = "
            DELETE FROM public.person
            WHERE first_name = :name
        ";
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $stmt = $connection->prepare($sql);
        $stmt->bindValue('name', $name);
        $stmt->execute();

    }

    /**
     * static function to getPerson by name
     * @param $name
     * @return mixed[]
     * @throws \Doctrine\DBAL\DBALException
     */

    static function getPersonByName($name)
    {
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $sql = "
              SELECT 
                p.*, 
                c.name as company 
              FROM public.person p 
              JOIN public.company c ON c.company_id = p.com_id
              WHERE p.first_name = ?
         ";

        $stmt = $connection->prepare($sql);
        $stmt->bindValue(1, $name);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * Statuc function to update person, this is used for edit person
     * @param $name
     * @throws \Doctrine\DBAL\DBALException
     */

    public function updatePerson($name)
    {

        $sql = "
            UPDATE public.person
            SET first_name = :first_name,
            last_name = :last_name,
            com_id = :com_id,
            phone = :phone,
            email = :email
            WHERE first_name = :old_name
        ";

        $com_id = Company::getCompanyIdbyName($this->company);

        echo(strval($com_id['company_id']));
        $db = new databaseConnection();
        $connection = $db->getConnection();

        $stmt = $connection->prepare($sql);
        $stmt->bindValue('first_name', $this->firstname);
        $stmt->bindValue('last_name', $this->lastname);
        $stmt->bindValue('com_id', $com_id['company_id']);
        $stmt->bindValue('phone', $this->phone);
        $stmt->bindValue('email', $this->email);
        $stmt->bindValue('old_name', $name);
        $stmt->execute();

    }


    /**
     * function to add person, this is used to add a new person in the database
     * @throws \Doctrine\DBAL\DBALException
     */

    public function addPerson()
    {
        $sql = "    
            INSERT INTO public.person(first_name, last_name, com_id, phone, email)    
            VALUES(    
              :first_name,    
              :last_name,    
              :com_id,        
              :phone,    
              :email   
            ) 
        ";

        echo($this->company);
        $com_id = Company::getCompanyIdbyName($this->company);

        echo(strval($com_id['company_id']));
        
        $db = new databaseConnection();
        $connection = $db->getConnection();
        
        $stmt = $connection->prepare($sql);
        $stmt->bindValue('first_name', $this->firstname);
        $stmt->bindValue('last_name', $this->lastname);
        $stmt->bindValue('com_id', $com_id['company_id']);
        $stmt->bindValue('phone', $this->phone);
        $stmt->bindValue('email', $this->email);
        $stmt->execute();
    }

    



    /**
     * Getters and Setters for all the entities
     */

    function getfirstname()
    {
        return $this->firstname;
    }

    function getlastname()
    {
        return $this->lastname;
    }

    function getphone()
    {
        return $this->phone;
    }

    function getemail()
    {
        return $this->email;
    }

    function getcompany()
    {
        return $this->company;
    }


    function setfirstname($new_first_name)
    {
        $this->firstname = (string) $new_first_name;
    }

    function setlastname($new_last_name)
    {
        $this->lastname = (string) $new_last_name;
    }
    function setemail($new_email)
    {
        $this->email= (string) $new_email;
    }
    function setcompany($new_company)
    {
        $this->company = (string) $new_company;
    }

    function setphone($new_phone)
    {
        $this->phone = (string) $new_phone;
    }

}












