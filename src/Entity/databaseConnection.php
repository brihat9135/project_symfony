<?php
/**
 * @author Brihat
 * Added this class so that we can import this class to connect to database for each function in other classes
 */

namespace App\Entity;

class databaseConnection
{

    public $config;
    public $connectionParams;

    function __construct() {
        $this->config = new \Doctrine\DBAL\Configuration();
        $this->connectionParams = array(
            'dbname' => 'customerDb',
            'user' => 'root',
            'password' => 'root',
            'host' => 'localhost',
            'driver' => 'pdo_pgsql',
        );
    }

    /**
     * @return \Doctrine\DBAL\Connection
     * @throws \Doctrine\DBAL\DBALException
     */
    function getConnection(){
        $connection= \Doctrine\DBAL\DriverManager::getConnection($this->connectionParams, $this->config);
        return $connection;
    }

}