<?php


/**
 * @author Brihat
 * Company entity which also does all the work of connecting to the database and getting require information
 */

namespace App\Entity;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\DriverManager;
use App\Entity\databaseConnection;

class Company
{
    protected $company_name;
    protected $address;
    protected $city;
    protected $state;
    protected $zip;
    protected $phone;




    /**
     * function to getcompany by its id
     * @return the postgres result from the database
     */


    static function getCompanybyId($id)
    {
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $sql = "SELECT * FROM public.company WHERE company_id=?";
        $stmt = $connection->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        return $stmt;
    }


    /**
     * static function to get company by name, this function is required to edit the company by user
     * @return array of postgres sql return
     */


    static function getCompanyByName($name)
    {
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $sql = "SELECT * FROM public.company WHERE name=?";
        $stmt = $connection->prepare($sql);
        $stmt->bindValue(1, $name);
        $stmt->execute();
        return$stmt->fetchAll();
    }

    static function getCompanyIdbyName($name)
    {
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $sql = "SELECT company_id FROM public.company WHERE name=?";
        $stmt = $connection->prepare($sql);
        $stmt->bindValue(1, $name);
        $stmt->execute();
        return$stmt->fetch();
    }

    /**
     * static function to get all the company
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    static function getAllCompany()
    {
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $users = $connection->fetchAll('SELECT * FROM public.company');
        return $users;

    }

    /**
     * static function to get all company name
     * @return mixed[]
     * @throws \Doctrine\DBAL\DBALException
     */

    static function getAllCompanyName()
    {
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $users = $connection->fetchAll('SELECT name FROM public.company');
        return $users;

    }


    /**
     * function to add new company in the database by the user
     */


    public function addCompany()
    {
        $sql = "
            INSERT INTO public.company(name, address, city, state, zip, phone)
            VALUES(
              :name,
              :address,
              :city,
              :state,
              :zip,
              :phone
            );
        ";
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $stmt = $connection->prepare($sql);
        $stmt->bindValue('name', $this->company_name);
        $stmt->bindValue('address', $this->address);
        $stmt->bindValue('city', $this->city);
        $stmt->bindValue('state', $this->state);
        $stmt->bindValue('zip', $this->zip);
        $stmt->bindValue('phone', $this->phone);
        $stmt->execute();
    }



    /**
     * function to update company by the user, used to edit the company
     */


    public function updateCompany($name)
    {
        $sql = "
            UPDATE public.company
            SET name = :name,
            address = :address,
            city = :city,
            state = :state,
            zip = :zip,
            phone = :phone
            WHERE name = :old_name
        ";
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $stmt = $connection->prepare($sql);
        $stmt->bindValue('name', $this->company_name);
        $stmt->bindValue('address', $this->address);
        $stmt->bindValue('city', $this->city);
        $stmt->bindValue('state', $this->state);
        $stmt->bindValue('zip', $this->zip);
        $stmt->bindValue('phone', $this->phone);
        $stmt->bindValue('old_name', $name);
        $stmt->execute();
    }



    /**
     * function to delete company by name, deletes the company in the database itself
     * need to fix this so the person also gets deleted while deleting the company in the database
     */



    static function deleteCompany($name)
    {
        $sql = "
            DELETE FROM public.company
            WHERE name = :name
        ";
        $db = new databaseConnection();
        $connection = $db->getConnection();
        $stmt = $connection->prepare($sql);
        $stmt->bindValue('name', $name);
        $stmt->execute();

    }


    /**
     * Getters and Setters for all the entities
     */

    function getCompanyName()
    {
        return $this->company_name;
    }

    function getAddress()
    {
        return $this->address;
    }
    function getCity()
    {
        return $this->city;
    }
    function getState()
    {
        return $this->state;
    }
    function getZip()
    {
        return $this->zip;
    }
    function getPhone()
    {
        return $this->phone;
    }
    function setCompanyName($new_company_name)
    {
        $this->company_name = (string) $new_company_name;
    }

    function setAddress($new_address)
    {
        $this->address = (string) $new_address;
    }
    function setCity($new_city)
    {
        $this->city = (string) $new_city;
    }
    function setState($new_state)
    {
        $this->state = (string) $new_state;
    }
    function setZip($new_zip)
    {
        $this->zip = (string) $new_zip;
    }
    function setPhone($new_phone)
    {
        $this->phone = (string) $new_phone;
    }

}

